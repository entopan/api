<?php namespace Entopancore\Api\Http;

use Entopancore\Api\Classes\Api;
use October\Rain\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('entopancore.api', function () {
            return Api::instance();
        });
        require_once 'helpers/ApiHelpers.php';

    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
}
