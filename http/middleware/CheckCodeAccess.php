<?php namespace Entopancore\Api\Http\Middleware;

use Closure;

class CheckCodeAccess
{
    public function handle($request, Closure $next)
    {
        if (get("code") == config('entopancore.api::code-access')) {
            return $next($request);
        } else {
            return response('Get token not valid', 403, ['message' => 'Get token not valid']);
        }
    }

}