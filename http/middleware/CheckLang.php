<?php namespace Entopancore\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Entopancore\Translate\Classes\TranslatorClass as Translator;

class CheckLang
{
    public function handle(Request $request, Closure $next)
    {
        if ($lang = $request->getPreferredLanguage()) {
            $translator = Translator::instance();
            $translator->setLocale($lang);
        } else {
            $lang = Translator::instance()->getLocale();
            $request->setLocale($lang);
        }
        return $next($request);


    }

}