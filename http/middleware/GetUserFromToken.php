<?php
/**
 * Created by PhpStorm.
 * User: francescopassanti
 * Date: 23/07/18
 * Time: 23:33
 */

namespace Entopancore\Api\Http\Middleware;


class GetUserFromToken extends \Tymon\JWTAuth\Middleware\GetUserFromToken
{


    public function handle($request, \Closure $next)
    {
        if (!$token = $this->auth->setRequest($request)->getToken()) {
            return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
        } catch (JWTException $e) {
            return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
        }


        if (!$user) {
            return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
        }

        $this->events->fire('tymon.jwt.valid', $user);

        $request->headers->set('user-id', $user->id);
        $request->headers->set('user', $user);

        return $next($request);
    }
}