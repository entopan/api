<?php namespace Entopancore\Api\Http\Middleware;

use Illuminate\Http\Request;

class BearerIsAuthenticated
{
    public function handle(Request $request, \Closure $next)
    {
        if ($user = $request->user()) {
            $request->headers->set('user-id', $user->id);
            $request->headers->set('is-authenticated', true);
            return $next($request);
        } else {
            $request->headers->set('is-authenticated', false);
            return $next($request);
        }

    }

}