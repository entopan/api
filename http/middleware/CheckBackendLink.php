<?php namespace Entopancore\Api\Http\Middleware;

use Closure;
use Backend\Facades\BackendAuth;

class CheckBackendLink
{
    public function handle($request, Closure $next)
    {
        if ((time() - 60 * 60 * 4) < decrypt(get('time')) && $user = BackendAuth::findUserByLogin(\Crypt::decrypt(get('login')))) {
            return $next($request);
        } else {
            return response(null, 403, ['message' => 'Unauthorized']);
        }
    }

}