<?php namespace Entopancore\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Api
{

    public function handle(Request $request, Closure $next)
    {
        $response = $request;

        $orderField = validateHeader($request->header('api-order-field'), "id");
        $orderType = validateHeader($request->header('api-order-type'), "asc");
        $apiType = validateHeader($request->header('api-type'), "take");
        $apiTake = validateHeader($request->header('api-take'), 15);
        $apiSkip = validateHeader($request->header('api-skip'), 0);
        $apiSelect = validateHeader($request->header('api-select'), '*');
        $apiParams = validateHeader($request->header('api-params'), []);
        $apiWhere = validateHeader($request->header('api-where'), null);
        $apiTerm = validateHeader($request->header('api-term'), null);


        $response->headers->set('api-type', $apiType);
        $response->headers->set('api-skip', $apiSkip);
        $response->headers->set('api-order-field', $orderField);
        $response->headers->set('api-order-type', $orderType);
        $response->headers->set('api-take', $apiTake);
        $response->headers->set('api-where', $apiWhere);
        $response->headers->set('api-params', $apiParams);
        $response->headers->set('api-select', $apiSelect);
        $response->headers->set('api-term', $apiTerm);

        return $next($response);

    }


}