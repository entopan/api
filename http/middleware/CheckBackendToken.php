<?php namespace Entopancore\Api\Http\Middleware;

use Backend\Models\User;
use Illuminate\Http\Request;


class CheckBackendToken
{

    public function handle(Request $request, \Closure $next)
    {
        if ($user = $this->checkUser($request->bearerToken())) {
            $request->headers->set('user-id', $user->id);
            $request->headers->set('user', $user);
            $request->headers->set('access-token', $request->bearerToken());
            return $next($request);
        } else {
            return response(null, 401, ['message' => 'User not authorized']);
        }

    }

    public function checkUser($token)
    {
        if (!$token) {
            return false;
        } else {
            return User::where('code', $token)->first();
        }
    }


}