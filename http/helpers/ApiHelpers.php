<?php

use GuzzleHttp\Client;

if (!function_exists('apiRouteBackend')) {

    function apiRouteBackend($url, $parameters = [])
    {
        if (is_array($parameters)) {
            if (count($parameters) > 0) {
                $url = \Twig::parse($url, $parameters);
            }
        }
        $query = parse_url($url, PHP_URL_QUERY);
        $user = \BackendAuth::getUser();

        if ($query) {
            $url .= '&time=' . \Crypt::encrypt(time()) . '&login=' . \Crypt::encrypt($user['login']);
        } else {
            $url .= '?time=' . \Crypt::encrypt(time()) . '&login=' . \Crypt::encrypt($user['login']);
        }
        return config("app.url") . config('entopancore.api::base_uri_backend') . $url;
    }
}

if (!function_exists('apiRoute')) {

    function apiRoute($url, $parameters = [], $token = false, $fileToken = false)
    {
        if (is_array($parameters)) {
            if (count($parameters) > 0) {
                $url = \Twig::parse($url, $parameters);
            }
        }
        $query = parse_url($url, PHP_URL_QUERY);

        if ($query) {
            $url .= '&platform-token=' . config('entopancore.api::token');
        } else {
            $url .= '?platform-token=' . config('entopancore.api::token');
        }

        if ($token) {
            $query = parse_url($url, PHP_URL_QUERY);
            if ($query) {
                $url .= '&access-token=' . $token;
            } else {
                $url .= '?access-token=' . $token;
            }
        }
        if ($fileToken) {
            $query = parse_url($url, PHP_URL_QUERY);
            if ($query) {
                $url .= '&file-token=' . $fileToken;
            } else {
                $url .= '?file-token=' . $fileToken;
            }
        }
        return config("app.url") . config('entopancore.api::base_uri') . $url;

    }
}

if (!function_exists('apiServer')) {
    function apiServer(\Illuminate\Http\Request $request, $baseUri = true)
    {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $request->header('access-token'),
                'User-Cookie' => $request->header('User-Cookie'),
                'User-Agent' => $request->header('User-Agent'),
                'Accept' => $request->header('Accept')
            ],
            'verify' => false,
            'http_errors' => false,
            'timeout' => 50.0,
        ];
        if ($baseUri) {
            $params['base_uri'] = config("app.url") . config('entopancore.api::base_uri');
        }
        return new Client($params);
    }
}


if (!function_exists('getSuccessResult')) {
    function getSuccessResult($data = null, $message = null, $header = array())
    {
        $headers = array();
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        $headers['message'] = ($message) ?? trans('entopancore.api::lang.message.ok');
        $headers['message'] = utf8_decode($headers['message']);
        $headers = array_merge($headers, $header);
        return response($data, 200, $headers);
    }
}

if (!function_exists('getCustomResult')) {
    function getCustomResult($code, $data = null, $message = null, $header = array())
    {
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        $headers = array();
        $headers['message'] = ($message) ?? null;
        $headers['message'] = utf8_decode($headers['message']);
        $headers = array_merge($headers, $header);

        return response($data, $code, $headers);
    }
}

if (!function_exists('getNotFoundResult')) {
    function getNotFoundResult($message = null)
    {
        $message = ($message) ?? trans('entopancore.api::lang.message.not_found');
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        $message = utf8_decode($message);
        return response(['message' => $message], 404, ['message' => $message]);
    }
}

if (!function_exists('getNoContentResult')) {
    function getNoContentResults($message = null)
    {
        $message = ($message) ?? trans('entopancore.api::lang.message.no_content_found');
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        $message = utf8_decode($message);
        return response(['message' => $message], 204, ['message' => $message]);
    }
}


if (!function_exists('getValidationResult')) {
    function getValidationResult($validation = null, $message = null)
    {
        $validationMessage = null;

        if ($validation) {
            $validation = json_decode($validation->messages(), true, 512);
            $validationMessage = implode(", ", array_flatten($validation));
            $validationMessage = utf8_decode($validationMessage);
        }

        return response($validation, 400, ['message' => ($validationMessage) ?? 'Dati non corretti', 'validation' => base64_encode($validationMessage)]);
    }
}


if (!function_exists('prepareRepositoryResponse')) {
    function prepareRepositoryResponse($status, $data, $message = null)
    {
        return [
            "status" => $status,
            "data" => $data,
            "message" => $message
        ];
    }
}


if (!function_exists('getErrorResult')) {
    function getErrorResult($message = null)
    {
        $message = ($message) ?? trans('entopancore.api::lang.message.error');
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        $message = utf8_decode($message);

        return response([], 500, ['message' => $message]);
    }
}

if (!function_exists('getNotAllowedResult')) {

    function getNotAllowedResult($message = null)
    {
        $message = ($message) ?? trans('entopancore.api::lang.message.not_allowed');
        if (is_array($message)) {
            $message = implode('\n', $message);
        }
        return response()->json(['message' => $message], 405);
    }
}

if (!function_exists('checkMethod')) {

    function checkMethod($array, $action, $method)
    {
        if (isset($array[$action])) {
            if (strtolower($array[$action]) == strtolower($method)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}


if (!function_exists('googleRecaptcha')) {
    function googleRecaptcha($data)
    {
        if (isset($data["g-recaptcha-response"])) {
            if ($reCaptcha = $data["g-recaptcha-response"]) {
                $response = apiServer($this->request)->post(
                    'https://www.google.com/recaptcha/api/siteverify', [
                        'form_params' =>
                            [
                                'secret' => config("services.captcha"),
                                'response' => $reCaptcha
                            ]
                    ]
                );
                if ($response->getStatusCode() !== 200) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }

    }
}


if (!function_exists('validateHeader')) {

    function validateHeader($value, $default = null)
    {
        if ($value == "" or is_null($value) or !$value or $value == "null" or $value == "undefined") {
            return $default;
        } else {
            return $value;
        }
    }
}


