<?php namespace Entopancore\Api;

use Entopancore\Api\Classes\ApiHelper;
use System\Classes\PluginBase;
use Illuminate\Support\Facades\App;
use Illuminate\Foundation\AliasLoader;


class Plugin extends PluginBase
{

    public function register()
    {
        App::register('Entopancore\Api\Http\ApiServiceProvider');

        App::singleton('api.helper', function () {
            return new ApiHelper();
        });

        AliasLoader::getInstance()->alias(ApiHelper::class, 'ApiHelper');
    }


}
