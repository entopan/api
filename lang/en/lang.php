<?php return [
    'plugin' => [
        'name' => 'Api',
        'description' => '',
    ],
    'api' => 'Api',
    'name' => 'Name',
    'id' => 'Id',
    'token' => 'Token',
    'date' => 'Date',
    'label' => 'Label',
    'access' => 'Access',
    'model' => 'Model',
    'model_path' => 'Model Path',
    'fields' => 'Fields',
    'is_active' => 'Is Active',
    'access_plugin' => 'Access plugin',
    'status' => [
        'ok' => 'ok',
        'error' => 'error',
        'validation' => 'validation',
    ],
    'message' => [
        'ok' => 'Success',
        'error' => 'Error',
        'validation' => 'Validation',
        'empty' => 'Empty',
        'not_found' => 'Not found',
        'access_not_valid' => 'Access not valid',
        'not_configured' => 'Not configured',
    ],
    'user_agent' => 'User agent',
    'ip' => 'Ip',
    'endpoint' => 'Endpoint',
    'info' => 'Info',
    'last_requested_date' => 'Last requested date',
    'attempts' => 'Attempts',
    'blacklists' => 'Blacklist',
];