<?php return [
    'plugin' => [
        'name' => 'Api',
        'description' => '',
    ],
    'api' => 'Api',
    'name' => 'Nome',
    'id' => 'Identificazione',
    'token' => 'Token',
    'date' => 'Data',
    'label' => 'Etichetta',
    'access' => 'Accesso',
    'model' => 'Modello',
    'model_path' => 'Percorso Modello',
    'fields' => 'Campi',
    'is_active' => 'È attivo?',
    'access_plugin' => 'Accesso plugin',
    'status' =>
        [
            'ok' => 'ok',
            'error' => 'error',
            'validation' => 'validation'
        ],
    'message' =>
        [
            'ok' => 'Richiesta inviata con succuesso.',
            'error' => 'Si è verificato un errore. Riprovare!',
            'validation' => 'Dati non corretti',
            'empty' => 'Vuoto',
            'not_found' => 'Not found',
            'no_content_found' => 'No content found',
            'access_not_valid' => 'Accesso non valido',
        ]
];