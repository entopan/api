<?php namespace Entopancore\Api\Facades;

use October\Rain\Support\Facade;

class Api extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'api.helper';
    }
}
