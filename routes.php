<?php

Route::group(
    [
        'prefix' => 'api/v1',
    ], function () {
    Route::options('{all}', ['uses' => 'Entopancore\Api\Http\Controllers\ApiController@all'])->where('all', '.*');
});