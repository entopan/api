<?php namespace Entopancore\Api\Classes;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MyRoutingController extends Controller
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    protected function fixParams($params)
    {
        $params = json_decode($params, true);
        $excluded = ["orderBy", "page", "term"];
        if ($params) {
            foreach ($params as $key => $param) {
                if (in_array($key, $excluded)) {
                    unset($params[$key]);
                } else {
                    $params[$key] = explode(",", $param);
                }
            }
            return $params;

        }
        return [];
    }

    protected function getLabelForCache($optional = [])
    {
        $label = $this->getCallingFunctionName();
        foreach ($this->request->headers->all() as $key => $header) {
            if (strpos($key, 'api') !== false) {
                $label .= array_first($header);
            }
        }
        if (count($optional) > 0) {
            $label .= implode("", $optional);
        }
        return base64_encode($label);
    }


    private function getCallingFunctionName($debug = false)
    {
        $r = '';
        $debugString = '';
        $caller = debug_backtrace();
        if ($debug === true) {
            $debugString .= ' <p>START: debug_backtrace result:</p>';
            $debugString .= '<pre>';
            $debugString .= print_r($caller, true);
            $debugString .= '</pre>';
            $debugString .= '<p>END: debug_backtrace result:</p>';
        }
        $caller = $caller[2];
        $r .= $caller['function'] . '()';
        if (isset($caller['class'])) {
            $r .= ' in ' . $caller['class'];
        }
        if (isset($caller['object'])) {
            $r .= ' (' . get_class($caller['object']) . ')';
        }
        return $r . $debugString;
    }
}